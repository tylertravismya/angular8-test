
// enum type CompanyType
export let CompanyType = {
	S_Corp:"S_Corp",
	LLC:"LLC",
	C_Corp:"C_Corp",
}
