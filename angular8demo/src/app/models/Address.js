var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Address
var Address = new Schema({
  street: {
	type : String
  },
  city: {
	type : String
  },
  state: {
	type : String
  },
  zipCode: {
	type : String
  },
},{
    collection: 'addresss'
});

module.exports = mongoose.model('Address', Address);